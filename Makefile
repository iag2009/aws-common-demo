shell = /bin/bash

.PHONY: build
build:
	@ cd src && \
		echo "AWS SQS and EC2 and Route53"

.PHONY: init
init: build
	@ terraform init

.PHONY: apply
apply: build
	@ terraform apply

.PHONY: destroy
destroy:
	@ terraform destroy