variable "region" {
  default = "us-east-2"
}

variable "ami" {
  description = "Depends on location"
  type        = string
  default     = "ami-01e7ca2ef94a0ae86"
}

variable "instance_name" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
  default     = "ExampleInstance"
}
