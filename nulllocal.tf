resource "null_resource" "command1" {
    provisioner "local-exec" {
        command = "echo $ENVIRONMENT $(date) $NAME1"
        environment = {
          NAME1 = "Vasja"
          NAME2 = "Petja"
        }
    }
}

resource "null_resource" "command2" {
    provisioner "local-exec" {
        command = "ping -c 5 www.google.com"
    }
    depends_on = [null_resource.command1]
}   

/*
resource "null_resource" "command3" {
    provisioner "local-exec" {
        command = "print ('Hello World')"
        interpreter = ["python", "-c"]
    }
    depends_on = [null_resource.command1, null_resource.command2]
}
*/ 
